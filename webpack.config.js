module.exports = {
  context: __dirname,
  entry: {
    actr: [
      './src/ActionCreator',
    ],
  },
  mode: 'none',
  module: {
    rules: [{
      exclude: /node_modules/,
      test: /\.js$/,
      use: 'babel-loader',
    }],
  },
  output: {
    path: __dirname + '/dist',
    filename: '[name].js',
    publicPath: '/',
    library: 'ActR',
    libraryTarget: 'umd',
  },
  resolve: {
    extensions: ['.js'],
  },
};
