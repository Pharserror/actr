(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["ActR"] = factory();
	else
		root["ActR"] = factory();
})(self, () => {
return /******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ([
/* 0 */,
/* 1 */
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }
function _defineProperty(obj, key, value) { key = _toPropertyKey(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return _typeof(key) === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (_typeof(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }
var defaultPlug = {
  naming: function naming(action) {
    // If the action is not a string then we throw an error
    if (action.constructor.name !== 'String') {
      throw 'Every action supplied to ActionCreator must be a string!';
    }
    // All actions should be in the format of "ON_SOME_EVENT"
    var actionEventName = 'ON';
    // We create some space to store the indexes of each capital character
    var indexes = [];
    // We look through the string and find and store the index of capital characters
    action.replace(/[A-Z]/g, function (match, index, actionName) {
      indexes.push(index);
    });
    // If there are no capital characters then we just capitalize the whole string
    if (indexes[0] === undefined) {
      actionEventName += '_' + action.toUpperCase();
    } else {
      /* Otherwise we capitalize the first part of an action like myAction
        * so that we get "_MY" */
      actionEventName += '_' + action.substr(0, indexes[0]).toUpperCase();
      /* Then for each index we found above we capitalize that part of the
        * string to "_ACTION" */
      indexes.forEach(function (index, i) {
        /* We want to check if we have reached the end of the string so that
          * we may properly calculate the length of the substr */
        var length = indexes[i + 1] === undefined ? action.length - index : indexes[i + 1] - index;
        actionEventName += '_' + action.substr(index, length).toUpperCase();
      });
      // We should now have an actionEventName like "ON_MY_ACTION"
    }

    return actionEventName;
  },
  /* destructor()
   *
   * @param action [String] A string like 'myAction'
   *
   * @param context [Function] this
   *
   * @param types [Any] Could be an Array, Object, or String
   *
   * @returns [null]
   */
  destructor: function destructor(action, context, types) {
    if (!!types) {
      switch (types.constructor.name) {
        case 'Array':
          {
            /* In this case our types are something like:
             * ['action1', 'action2', ...]
             *
             * And we want to return something like:
             * { action1: options => { ...options, type: 'action1' } }
             */
            types.map(function (type) {
              context[action] = _defineProperty({}, type, function (options) {
                return _objectSpread(_objectSpread({}, options), {}, {
                  type: type
                });
              });
            });
            break;
          }
        case 'Object':
          {
            /* In this case our types should be something like:
             * {
             *   FAILURE: 'my-action/FAILURE',
             *   SUCCESS: 'my-action/SUCCESS'
             * }
             *
             * And we will return something like this:
             * { SUCCESS: options => { ...options, type: 'my-action/SUCCESS' } }
             *
             * And then it may be called like this:
             * actions.myAction.SUCCESS(options);
             */
            Object.keys(types).map(function (type) {
              context[action] = _objectSpread(_objectSpread({}, context[action] || {}), {}, _defineProperty({}, type, function () {
                var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
                return _objectSpread(_objectSpread({}, options), {}, {
                  type: types[type]
                });
              }));
            });
            break;
          }
        case 'String':
          {
            /* Should give us something like this:
             * { myAction: options => ({ ...options, type: 'ON_MY_ACTION' })
             */
            context[action] = function (options) {
              return _objectSpread(_objectSpread({}, options), {}, {
                type: types
              });
            };
            break;
          }
      }
    } else {
      throw 'You must supply action types!';
    }
  }
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (defaultPlug);

/***/ })
/******/ 	]);
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ ActionCreator)
/* harmony export */ });
/* harmony import */ var _defaultPlug__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(1);
function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }
function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, _toPropertyKey(descriptor.key), descriptor); } }
function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return _typeof(key) === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (_typeof(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }


/* ActionCreator
 *
 * Docs have moved to the README.md
 */
var ActionCreator = /*#__PURE__*/_createClass(
/* constructor()
 *
 * @param actions [Array] An array of strings to be used for action names
 *
 * @param options [Object] Any plugins to be used should be passed as { plug: MyPlug }
 *
 * @returns [ActionCreator]
 */
function ActionCreator(actions) {
  var _this = this;
  var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  _classCallCheck(this, ActionCreator);
  // Actions needs to be an array of strings
  if (actions.constructor.name !== 'Array') {
    throw 'You must supply an array of actions to ActionCreator!';
  } else {
    actions.forEach(function (action) {
      /* For each action we pass it to a plugin for naming in-case the user
       * has their own scheme they would rather use than the default
       *
       * Plugs are free to return an array, object, or string - they may opt
       * to then use either the built-in destructor or provide one themselves
       */
      var types = options.plug && options.plug.naming ? options.plug.naming(action, options.plugOptions) : _defaultPlug__WEBPACK_IMPORTED_MODULE_0__["default"].naming(action);
      options.plug && options.plug.destructor ? options.destructor(action, _this, types) : function () {
        _defaultPlug__WEBPACK_IMPORTED_MODULE_0__["default"].destructor(action, _this, types);
      }();
    }, this);
  }
});

})();

/******/ 	return __webpack_exports__;
/******/ })()
;
});